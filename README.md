# dot

## Hacking with Spark

- read from [here](https://medium.com/@itseranga/hacking-with-apache-spark-f6b0cabf0703)


## Hacking with Spark DataFrame

- read from [here](https://medium.com/@itseranga/hacking-with-spark-dataframe-d717404c5812)


## Spark Cassandra Connector

- read from [here](https://medium.com/@itseranga/spark-cassandra-connector-24e5c8c7a03c)


## K-Means clustering with Apache Spark

- read from [here](https://medium.com/@itseranga/k-means-clustering-with-apache-spark-cab44aef0a16)


## Logistic Regression with Apache Spark

- read from [here](https://medium.com/rahasak/logistic-regression-with-apache-spark-b7ec4c98cfcd)


## Random Forest Classifier with Apache Spark

- read from [here](https://medium.com/@itseranga/random-forest-classifier-with-apache-spark-c63b4a23a7cc)


## Collaborative Filtering-based Book Recommendation System With Spark-ML and Scala

- read from [here](https://medium.com/rahasak/collaborative-filtering-based-book-recommendation-system-with-spark-ml-and-scala-1e5980ceba5e)
